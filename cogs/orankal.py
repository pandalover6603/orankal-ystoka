import discord
import subprocess
import sys

from discord import app_commands
from discord.ext import commands

from util.timeout import timeout

class ManageCog(commands.Cog, name="Manage"):
    def __init__(self, bot):
        self.bot = bot
        self.r = subprocess.Popen(["orankal/orankal.py"])
        self.auth = [ # list of allowed users
            689166241369161874, # dublUayaychtee#9410
        ]
        self.resetConfirm = 0
        self.quitConfirm = 0

    @commands.command(name="start")
    async def start(self, ctx: discord.Interaction):
        """
        Starts the bot
        """
        if ctx.author.id not in self.auth:
            await ctx.send("can't sorry")
            return

        if self.r.poll() is None:
            await ctx.send("Process is already running!")
            return

        self.r = subprocess.Popen(["orankal/orankal.py"])
        await ctx.send("Started!")

    @commands.command(name="stop")
    async def stop(self, ctx: discord.Interaction):
        """
        Waits for the bot to exit.

        The stop command in the bot makes the bot close itself, and this command just waits for it to finish exiting.
        """
        if ctx.author.id not in self.auth:
            await ctx.send("can't sorry (manager)")
            return

        with timeout(seconds=4):
            self.r.wait()
            await ctx.send("Exited.")
            return
        await ctx.send("bruh (timeout)")

    @commands.command(name="kill")
    async def kill(self, ctx: discord.Interaction):
        """
        Force the bot to exit.

        This command forces the bot to stop, instead of waiting for it to exit.
        """
        if ctx.author.id not in self.auth:
            await ctx.send("can't sorry (manager)")
            return

        self.r.kill()
        await ctx.send("Killed process.")

    @commands.command(name="update")
    async def restart(self, ctx: discord.Interaction):
        """
        Starts the bot again after it updates and exits.
        """
        if ctx.author.id not in self.auth:
            await ctx.send("can't sorry")
            return



        with timeout(seconds=4):
            self.r.wait()
            await ctx.send("Exited.")
            try:
                await ctx.send("Waiting to exit...")
                self.r.wait()
                self.r = subprocess.Popen(["orankal/orankal.py"])
                await ctx.send("Started!")
            except subprocess.CalledProcessError as e:
                await ctx.send("Error:```" + e.output + "```")
            return
        await ctx.send("bruh (timeout)")

    @commands.command(name="reset")
    async def reset(self, ctx: discord.Interaction):
        """
        TODO: make reset script to restart the manager.
        """
        if ctx.author.id not in self.auth:
            await ctx.send("can't sorry")
            return

        if not self.resetConfirm:
            self.resetConfirm = ctx.author.id
            await ctx.send("Type the command again to confirm")
            return

        if self.resetConfirm == ctx.author.id:
            # TODO start reset script
            await ctx.send("Exiting... (manager)")
            sys.exit()
            return

        self.resetConfirm = 0
        await ctx.send("Not the same user or unknown error. Please try again.")

    @commands.command(name="quit")
    async def quit(self, ctx: discord.Interaction):
        """
        Make the manager exit. (WARNING: Stops the bot and the manager, the bot will have to be started again manually.)
        """

        if ctx.author.id not in self.auth:
            await ctx.send("can't sorry")
            return

        if not self.resetConfirm:
            self.quitConfirm = ctx.author.id
            await ctx.send("Type the command again to confirm")
            return

        if self.quitConfirm == ctx.author.id:
            # TODO start reset script
            await ctx.send("Exiting... (manager)")
            sys.exit()
            return

        self.quitConfirm = 0
        await ctx.send("Not the same user or unknown error. Please try again.")

async def setup(bot):
    await bot.add_cog(ManageCog(bot))
