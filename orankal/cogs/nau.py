#!/usr/bin/env python3

from __future__ import print_function

import io
import google.auth
import os.path
import shutil

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

import discord
import subprocess
import sys
import zipfile
import sqlite3
import re
import json

from dotenv import load_dotenv
from discord import app_commands
from discord.ext import commands
from math import ceil

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']


class NauCog(commands.Cog, name="Nau"):
    def __init__(self, bot):
        self.bot = bot
        self.creds = None

        if os.path.exists('token.json'):
            self.creds = Credentials.from_authorized_user_file(
                'token.json', SCOPES)

        if not self.creds or not self.creds.valid:
            if self.creds and self.creds.expired and self.creds.refresh_token:
                self.creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                self.creds = flow.run_local_server(port=0)
                # Save the credentials for the next run
                with open('token.json', 'w') as token:
                    token.write(self.creds.to_json())

        self.nauid = "1q8GwYPFhsZyeYijTrH-kTY-EqR25EHs5h-syjC2Y_YU"

        self.service = build('sheets', 'v4', credentials=self.creds)
        self.sheet = self.service.spreadsheets()
        self.query = ""

        self.lastSearch = []
        self.con = sqlite3.connect("data/db.sqlite3")
        self.cur = self.con.cursor()
        self.cur.execute(
        """
        CREATE TABLE IF NOT EXISTS "nau" (
            [id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            [part] TEXT NOT NULL,
            [word] TEXT NOT NULL,
            [transl] TEXT NOT NULL,
            [desc] TEXT,
            [etym] TEXT,
            [rel] TEXT
        );
        """
        )

    @commands.command(name="pull")
    async def pull(self, ctx: discord.Interaction):
        """
        Refresh the list of words

        Downloads words from the google doc.
        """

        await ctx.send("Getting values...")
        result = self.sheet.values().get(spreadsheetId=self.nauid,
                                    range='Nau e kenau!A:F',
                                    valueRenderOption='FORMATTED_VALUE').execute()
        values = result.get('values', [])
        
        if not values:
            await ctx.send("the words are gone!")
            return
        
        await ctx.send("Database ukïmo...")
        self.cur.execute("DELETE FROM nau;")
        self.cur.execute("DELETE FROM sqlite_sequence;")

        await ctx.send("Database ðï naun čosaþ...")
        for row in values:
            row += [''] * (6 - len(row))
            if row[0] and row[1] and row[2]:
                self.cur.execute("INSERT INTO nau (part, word, transl, desc, etym, rel) VALUES (?, ?, ?, ?, ?, ?)", row)
            elif row[1]:
                await ctx.send("Invalid row: " + row[1])

        self.con.commit()
        await ctx.send("Done!")

    @commands.command(name="desc")
    async def desc(self, ctx: discord.Interaction, id):
        """
        Get full description of word from id number

        Usage: !desc <id>
        """
        self.cur.execute("SELECT * FROM nau WHERE id = ?", (id,))
        word = self.cur.fetchone()

        resultstring =  f"``` {str(word[0])} | {word[1]} : {word[2]} - {word[3]}"
        if word[4]:
            resultstring += f"{word[4]}"
        else:
            resultstring += "\nNo description provided."
        if word[5]:
            resultstring += f"{word[5]}"
        if word[6]:
            rel = json.loads(word[6])
            resultstring += "\n---"
            for i, v in rel[0].items():
                resultstring += f"\n{i} - {v}"
        resultstring += "```"

        await ctx.send(resultstring)


    @commands.command(name="ije")
    async def ije(self, ctx: discord.Interaction, *, arg):
        """
        Search the list of Ijeða words

        Usage: !ije <search query>
        """
        self.query = arg.lower()

        self.cur.execute("SELECT id, word, part, transl FROM nau WHERE word LIKE ? ORDER BY id", ('%' + arg.lower() + '%',))
        results = self.cur.fetchall()

        if len(results) == 0:
            await ctx.send(f"```No results for: {arg}```")
            return

        maxlength = 0
        for result in results:
            if len(result[1]) > maxlength:
                maxlength = len(result[1])

        self.lastSearch = []
        for result in results:
            self.lastSearch.append(f"{str(result[0]).rjust(3)} | {result[1].ljust(maxlength)} : {result[2].ljust(7)} - {result[3]}")

        await self.page(ctx, "1")

    @commands.command(name="eng")
    async def eng(self, ctx: discord.Interaction, *, arg):
        """
        Search the list of English words

        Usage: !eng <search query>
        """
        self.query = arg.lower()

        self.cur.execute("SELECT id, word, part, transl FROM nau WHERE transl LIKE ? ORDER BY id", ('%' + arg + '%',))
        results = self.cur.fetchall()

        if len(results) == 0:
            await ctx.send(f"```No results for: {arg}```")
            return

        maxlength = 0
        for result in results:
            if len(result[1]) > maxlength:
                maxlength = len(result[1])

        self.lastSearch = []
        for result in results:
            self.lastSearch.append(f"{str(result[0]).rjust(3)} | {result[1].ljust(maxlength)} : {result[2].ljust(7)} - {result[3]}")

        await self.page(ctx, "1")

    @commands.command(name="page")
    async def page(self, ctx: discord.Interaction, page):
        """
        See x page of search results

        Usage: !page <page number>
        """
        if page.isdigit():
            resultstring = f"```Results for: {self.query}\n"
            page = int(page)
            resultstring += '\n'.join(self.lastSearch[(page - 1) * 10 : (page - 1) * 10 + 10])
            resultstring += f"\n{(page - 1) * 10}-{(page - 1) * 10 + 10}/{len(self.lastSearch)}   page {page}/{ceil(len(self.lastSearch)/10)}"
            resultstring += "```"

            await ctx.send(resultstring)
            return

        await ctx.send("Invalid page or unknown error")

async def setup(bot):
    await bot.add_cog(NauCog(bot))
