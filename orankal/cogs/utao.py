import discord
import subprocess
import sys

from discord import app_commands
from discord.ext import commands

class UtaoCog(commands.Cog, name="Utao"):
    def __init__(self, bot):
        self.bot = bot
        self.auth = [ # list of allowed users
            689166241369161874, # dublUayaychtee#9410
        ]

    @commands.command(name="crag")
    async def crag(self, ctx: discord.Interaction):
        """
        Runs `git status` and returns the output.
        """
        try:
            await ctx.send("```" + subprocess.check_output(["git", "status"]).decode('utf-8') + "```")
        except subprocess.CalledProcessError as e:
            await ctx.send("Error:```" + e.output + "```")

    @commands.command(name="branch")
    async def branch(self, ctx: discord.Interaction):
        """
        Gets branch information.
        """
        try:
            await ctx.send("```" + subprocess.check_output(["git", "branch"]).decode("utf-8") + "```")
        except subprocess.CalledProcessError as e:
            await ctx.send("Error:```" + e.output + "```")

    @commands.command(name="update")
    async def update(self, ctx: discord.Interaction):
        """
        Update the bot and exit. (Started again by the manager)
        """
        if ctx.author.id not in self.auth:
            await ctx.send("can't sorry")
            return

        try:
            output = subprocess.check_output(["git", "pull", "-f"]).decode("utf-8")
            await ctx.send("```" + output + "```")
            await ctx.send("Exiting...")
            sys.exit()
        except subprocess.CalledProcessError as e:
            await ctx.send("Error:```" + e.output + "```")

    @commands.command(name="stop")
    async def stop(self, ctx: discord.Interaction):
        """
        Stop the bot. (The manager will keep running)
        """
        if ctx.author.id not in self.auth:
            await ctx.send("can't sorry (orankal)")
            return

        await ctx.send("Exiting...")
        sys.exit()


async def setup(bot):
    await bot.add_cog(UtaoCog(bot))
