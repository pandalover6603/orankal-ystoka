from math import floor, ceil
from convertdate import gregorian

MONTHS = [
    "Seðïsa",
    "Ïsto",
    "Šomi",
    "Þaša",
    "Koža",
]

WEEKDAYS = [
    "Ukeð",
    "Tersï",
    "Naki",
    "Sosaš",
    "Zeža",
    "Kasað",
    "Mešto",
    "Ïntaia",
    "Ðesoðo",
    "Sasað",
    "Þareið",
    "Retað",
]

EPOCH = 2460025.5

def to_jd(year, month, day):
    "Retrieve the Julian date equivalent for this date"
    return day + (month - 1) * 73 + (year) * 365 + ceil(year / 4) + EPOCH - 1


def from_jd(jdc):
    "Create a new date from a Julian date."
    cdc = floor(jdc) + 0.5 - EPOCH
    year = floor((cdc - ceil((cdc - 1) / 1461)) / 365)

    yday = jdc - to_jd(year, 1, 1)

    month = floor(yday / 73) + 1
    day = yday - (month - 1) * 73 + 1

    return int(year), int(month), int(day)

def to_gregorian(year, month, day):
    return gregorian.from_jd(to_jd(year, month, day))

def from_gregorian(year, month, day):
    return from_jd(gregorian.to_jd(year, month, day))

def date(year, month, day, cross=False):
    """
    If cross is True, replace month and day with '-' on leap day.
    """
    if cross and month == 6:
        return f"{str(year).rjust(3, '0')}/-/--"
    return f"{str(year).rjust(3, '0')}/{month}/{str(day).rjust(2, '0')}"

def dateText(year, month, day):
    monthname = MONTHS[month - 1]
    if month == 6:
        return f"insert leap day name, {str(year).rjust(3, '0')}"
    if day == 73:
        weekday = f"Woðos"
    else:
        weekday = WEEKDAYS[(day - 1) % 12]

    return f"{weekday}, {str(day).rjust(2, '0')} {monthname}, {str(year).rjust(3, '0')}"

def print_array(array):
    arrdict = {'{}'.format(i): x for i,x in enumerate(array)}
    return '\n'.join('{}: {}'.format(key, val) for key, val in arrdict.items())

def get_months():
    return print_array(MONTHS)

def get_weekdays():
    return print_array(WEEKDAYS)

def date_from_gregorian(year, month, day):
    y, m, d = from_gregorian(year, month, day)
    return date(y, m, d)

def dateText_from_gregorian(year, month, day):
    y, m, d = from_gregorian(year, month, day)
    return dateText(y, m, d)
